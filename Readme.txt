ENEL 487 Assignment 5
Here we were to compute a lookup table, and then run comparisons of it vs the sin function directly.

These are the results of the average of 720 runs, ie, once for every value of sine that we put into the lookup table.

Output on my laptop using Visual Studio:
Delta t2-t1 of an empty loop: 26.6486 nanoseconds
Delta t4-t3 - (t2-t1) of using the lookup table: 193.982 nanoseconds
Delta t5-t6 - (t2-t1) of computing sine directly: 281.368 nanoseconds

Output on Snoopy using g++:
Delta t2-t1 of an empty loop: 8.43611 nanoseconds
Delta t4-t3 - (t2-t1) of using the lookup table: 10.5833 nanoseconds
Delta t5-t6 - (t2-t1) of computing sine directly: 142.436 nanoseconds


The program file is Main.cpp
The makefile is Makefile

The rest of the assignment is in A5.pdf

