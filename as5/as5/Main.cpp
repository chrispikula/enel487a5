/**
Author: Chris Pikula
ENEL 487 A5.
This is a small application that tests a lookup table, vs calling the sine functions.
*/

#include <iostream>
#include <chrono>
#include <string>
#include <math.h>

class LookupTable 
{
public:
	int sineValues[360 * 2 * 2] = { 0 };
	int returnValue(double degrees);
};

#define PI 3.14159265
using std::cout;
using std::endl;
using std::cerr;

typedef std::chrono::high_resolution_clock Clock;

int main()
{
	LookupTable Lookup_Table;
	//precompute lookup table;
	volatile int y = 0;
	volatile double z = 0;
	for (double x = -359.5; x < 359.9; x = x + .5)
	{
		Lookup_Table.sineValues[y] = int(100000 * sin(x*PI/180));
		//cerr << Lookup_Table.sineValues[y] << endl << y << endl;
		y++;
	}

	/**
	debugging
	for (float x = -359.5; x < 359.9; x = x + .5)
	{
		cerr << Lookup_Table.returnValue(x) << endl << x << endl;
	}
	*/

	//calculate empty loop
	auto t1 = Clock::now();
	y = 0;
	for (double x = -359.5; x < 359.9; x = x + .5)
	{
		y = 35;
	}
	auto t2 = Clock::now();
	cout << "Delta t2-t1 of an empty loop: "
		<< float(std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count())/float(720) 
		<< " nanoseconds" << endl;


	//TEST lookup table 720 times
	auto t3 = Clock::now();
	for (double x = -359.5; x < 359.9; x = x + .5)
	{
		y = Lookup_Table.returnValue(x);
	}
	auto t4 = Clock::now();
	cout << "Delta t4-t3 - (t2-t1) of using the lookup table: "
		<< float(std::chrono::duration_cast<std::chrono::nanoseconds>(t4 - t3).count() -
			std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count()) / float(720)
		<< " nanoseconds" << endl;

	//TEST sin 720 times
	auto t5 = Clock::now();
	for (double x = -359.5; x < 359.9; x = x + .5)
	{
		z = sin(x);
	}
	auto t6 = Clock::now();
	std::cout << "Delta t5-t6 - (t2-t1) of computing sine directly: "
		<< float(std::chrono::duration_cast<std::chrono::nanoseconds>(t6 - t5).count() -
		std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count())/float(720)
		<< " nanoseconds" << std::endl;

	y = int(z);

	return 0;


}

int LookupTable::returnValue(double degrees)
{
	
	while (degrees < -360)
	{
		degrees = degrees + 360;
	}
	while (degrees > 360)
	{
		degrees = degrees - 360;
	}
	degrees = (degrees + 359.5) * 2;
	
	return sineValues[int(degrees)];
}
